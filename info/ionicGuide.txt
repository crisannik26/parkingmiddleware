Documentazione http://ionicframework.com/docs/api/

pagine in src/pages/...ecc

per aggiungere una pagina -> ionic g page pageName

IMPORTANTE aggiungere le pagine create ad app.module.ts(non usare il module.ts generato, a meno che annikmultimargial non sa come funziona)!!!

dentro typescript troviamo:
•	 Modules
•	 Decorator
•	 Class

i moduli sono import di componenti angular, ionic, cordova ecc

i decorator sono:
•	@Component  The @Component decorator simply defines that the class, and
	its associated template, will be published as a component. (built-in ionic component http://ionicframework.com/docs/components/#action-sheets)
•	@Directive    When you are looking to modify the behaviour of an existing component then you would create your own custom directives.
•	@Pipe   This decorator is used when creating classes to handle the filtering of data within your app template.
•	@Injectable    This decorator defines a provider - also known as a service.



Lifecycle events

•	 ionViewDidLoad (triggered only once per page after the page has completed loading and is now the active page)
•	 ionViewWillEnter (run when the requested page is about to enter and become the active page)
•	 ionViewDidEnter (run when the page has completed entering and is now the active page)
•	 ionViewWillLeave (run when the page is about to leave and no longer be the active page)
•	 ionViewDidLeave (run when the previously active page has completed leaving)
•	 ionViewWillUnload (run when the page is about to be destroyed and all of its elements removed)
•	 ionViewCanEnter (runs before the page view can enter)
•	 ionViewCanLeave (runs before the page view can leave)


Change root:
this.nav.setRoot(whateverPageYouWantToBeTheRootPage);

http://ionicframework.com/docs/v2/api/components/nav/NavController/
http://ionicframework.com/docs/v2/api/components/menu/Menu/
http://ionicframework.com/docs/v2/api/components/tabs/Tabs/




