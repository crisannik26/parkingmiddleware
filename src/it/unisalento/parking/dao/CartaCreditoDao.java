package it.unisalento.parking.dao;

import java.util.List;

import it.unisalento.parking.domain.CartaCredito;

public interface CartaCreditoDao extends BaseDao<CartaCredito> {
	public List<CartaCredito> allcard(int iduser);

}
