package it.unisalento.parking.dao;

import it.unisalento.parking.domain.Ausiliario;

public interface AusiliarioDao extends BaseDao<Ausiliario> {
	public abstract Ausiliario extractinfo(int id);


}
