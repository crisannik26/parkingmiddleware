/**
 * 
 */
package it.unisalento.parking.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import it.unisalento.parking.dao.MultaDao;
import it.unisalento.parking.domain.Multa;
import it.unisalento.parking.util.HibernateUtil;
/**
 * @author joker
 *
 */
public class MultaDaoImpl extends BaseDaoImpl<Multa> implements MultaDao{
	String tabella = Multa.class.getName();
	
	public List<Multa> getMulteByIdAus(int idaus){
		Session session = HibernateUtil.openSession();
		Transaction tx = session.beginTransaction();
		List<Multa> lista_multa= (List<Multa>) session.createQuery("from "+tabella+" where Ausiliario_idAusiliario=:id").setParameter("id", idaus).list();
		
		tx.commit();
		session.close();
		return lista_multa;
	}
	
	
	@Override
	public List<Multa> getMulteByIdUser(int iduser) {
		Session session = HibernateUtil.openSession();
		Transaction tx = session.beginTransaction();
		List<Multa> lista_multa= (List<Multa>) session.createQuery("from "+tabella+" where Utente_idUtente=:id").setParameter("id", iduser).list();
		
		tx.commit();
		session.close();
		return lista_multa;
	}
	
	

}
