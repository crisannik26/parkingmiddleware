/**
 * 
 */
package it.unisalento.parking.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import it.unisalento.parking.dao.CartaCreditoDao;
import it.unisalento.parking.domain.CartaCredito;
import it.unisalento.parking.util.HibernateUtil;

/**
 * @author joker
 *
 */
public class CartaCreditoDaoImpl extends BaseDaoImpl<CartaCredito> implements CartaCreditoDao{
	String tabella = CartaCredito.class.getName();
	public List<CartaCredito> allcard(int iduser){
		Session session = HibernateUtil.openSession();
		Transaction tx = session.beginTransaction();
		List<CartaCredito> card = (List<CartaCredito>) session.createQuery("from "+tabella+" where Utente_idUtente=:id").setParameter("id", iduser).list();
		tx.commit();
		session.close();
		return card;
	}

}
