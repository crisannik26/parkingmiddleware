/**
 * 
 */
package it.unisalento.parking.dao.impl;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import it.unisalento.parking.dao.SegnalaDao;
import it.unisalento.parking.domain.Segnala;
import it.unisalento.parking.util.HibernateUtil;

/**
 * @author joker
 *
 */
public class SegnalaDaoImpl extends BaseDaoImpl<Segnala> implements SegnalaDao{
	String tabella=Segnala.class.getName();
	public Segnala extractinfo(int idsegnalazione){
		Session session = HibernateUtil.openSession();
		Transaction tx = session.beginTransaction();
		Segnala info = (Segnala) session.createQuery("from "+tabella+" where Segnalazione_idSegnalazione=:id").setParameter("id", idsegnalazione).uniqueResult();
		tx.commit();
		session.close();
		return info;
	}

}
