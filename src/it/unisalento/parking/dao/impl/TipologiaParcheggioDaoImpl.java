/**
 * 
 */
package it.unisalento.parking.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import it.unisalento.parking.dao.FactoryDao;
import it.unisalento.parking.dao.TipologiaParcheggioDao;
import it.unisalento.parking.domain.TipologiaParcheggio;
import it.unisalento.parking.util.HibernateUtil;

/**
 * @author joker
 *
 */
public class TipologiaParcheggioDaoImpl extends BaseDaoImpl<TipologiaParcheggio> implements TipologiaParcheggioDao{
	String tabella= TipologiaParcheggio.class.getName();
	
	public List<TipologiaParcheggio> extractinfo(int idzone){
		Session session = HibernateUtil.openSession();
		Transaction tx = session.beginTransaction();
		List<TipologiaParcheggio> list_tp= (List<TipologiaParcheggio>) session.createQuery("from "+tabella+" where Zona_idZona=:idzone").setParameter("idzone",idzone).list();
		tx.commit();
		session.close();

		return list_tp;
	}
	
	public int NumberPlace(int idzone){
		int number=0;
		Session session = HibernateUtil.openSession();
		Transaction tx = session.beginTransaction();
		List<TipologiaParcheggio> list_tp= (List<TipologiaParcheggio>) session.createQuery("from "+tabella+" where Zona_idZona=:idzone").setParameter("idzone",idzone).list();
		tx.commit();
		session.close();
		for(int i=0; i<list_tp.size();i++){
			number=number+list_tp.get(i).getNumeroPosti();
		}
		
		return number;
		
	}
	
	public TipologiaParcheggio extractinfotype(int idzone, int idtype){
		Session session = HibernateUtil.openSession();
		Transaction tx = session.beginTransaction();
		TipologiaParcheggio type=(TipologiaParcheggio) session.createQuery("from "+tabella+" where Zona_idZona=:idzone and idTipologia_parcheggio=:idtype")
				.setParameter("idzone", idzone).setParameter("idtype", idtype).uniqueResult();
		tx.commit();
		session.close();
		return type;
	}

}
