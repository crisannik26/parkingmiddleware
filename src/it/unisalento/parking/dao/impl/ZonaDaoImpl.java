/**
 * 
 */
package it.unisalento.parking.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import it.unisalento.parking.dao.FactoryDao;
import it.unisalento.parking.dao.ZonaDao;
import it.unisalento.parking.domain.Zona;
import it.unisalento.parking.util.HibernateUtil;
/**
 * @author joker
 *
 */
public class ZonaDaoImpl extends BaseDaoImpl<Zona> implements ZonaDao{
	String tabella=Zona.class.getName();
	@Override
	public Zona idaftercreate() {
		//MI PERMETTE DI ESTRAPOLARE L'ULTIMO ID INSERITO NELLA TABELLA ZONA
		Session session = HibernateUtil.openSession();
		Transaction tx = session.beginTransaction();
		Zona zone = (Zona) session.createQuery("from "+tabella+" order by idZona desc").setMaxResults(1).uniqueResult();
		tx.commit();
		session.close();
		return zone;
	}
	@Override
	public List<Zona> extractinfo(int idgestore) {
		// TODO Auto-generated method stub
		Session session = HibernateUtil.openSession();
		Transaction tx = session.beginTransaction();
		List<Zona> list = (List<Zona>) session.createQuery("from "+tabella+" where Gestore_idGestore=:idgestore order by nome").setParameter("idgestore", idgestore).list();
		tx.commit();
		session.close();
		return list;
	}
	

}
