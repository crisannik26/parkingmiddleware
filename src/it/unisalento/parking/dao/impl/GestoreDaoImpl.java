/**
 * 
 */
package it.unisalento.parking.dao.impl;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import it.unisalento.parking.dao.GestoreDao;
import it.unisalento.parking.domain.Ausiliario;
import it.unisalento.parking.domain.Gestore;
import it.unisalento.parking.util.HibernateUtil;

/**
 * @author joker
 *
 */
public class GestoreDaoImpl extends BaseDaoImpl<Gestore> implements GestoreDao {
	public Gestore extractinfo(int id){
			Session session = HibernateUtil.openSession();
			Transaction tx =session.beginTransaction();
			Gestore gestore = (Gestore) session.createQuery("from "+Gestore.class.getName()+" where Persona_idPersona=:id").setParameter("id", id).uniqueResult();
			tx.commit();
			session.close();
			return gestore;
				
	}

}
