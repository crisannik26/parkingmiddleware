/**
 * 
 */
package it.unisalento.parking.dao.impl;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import it.unisalento.parking.dao.PersonaDao;
import it.unisalento.parking.domain.Persona;
import it.unisalento.parking.util.HibernateUtil;

/**
 * @author joker
 *
 */
public class PersonaDaoImpl extends BaseDaoImpl<Persona> implements PersonaDao {

	/* (non-Javadoc)
	 * @see it.unisalento.parking.dao.PersonaDao#getByAll()
	 */
	String tabella= Persona.class.getName();
	public Persona login(String mail) {
			Session session=HibernateUtil.openSession();
			Transaction tx=session.beginTransaction();
			Persona persona =(Persona) session.createQuery("from "+tabella+" where mail=:mail").setParameter("mail", mail).uniqueResult();
			tx.commit();
			session.close();
			return persona;
		
	}
	
	public Persona getid(String mail){
		
		Session session = HibernateUtil.openSession();
		Transaction tx = session.beginTransaction();
		Persona id = (Persona) session.createQuery("from "+ tabella+" where mail=:mail").setParameter("mail", mail).uniqueResult();
		tx.commit();
		session.close();
		return id;
	}

}
