/**
 * 
 */
package it.unisalento.parking.dao.impl;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import it.unisalento.parking.dao.FactoryDao;
import it.unisalento.parking.dao.UtenteDao;
import it.unisalento.parking.domain.Utente;
import it.unisalento.parking.util.HibernateUtil;
/**
 * @author joker
 *
 */
public class UtenteDaoImpl extends BaseDaoImpl<Utente> implements UtenteDao{
	String tabella = Utente.class.getName();
	public Utente extractinfo(int idpersona){
			Session session = HibernateUtil.openSession();
			Transaction tx =session.beginTransaction();
			Utente user = (Utente) session.createQuery("from "+tabella+" where Persona_idPersona=:id").setParameter("id", idpersona).uniqueResult();
			tx.commit();
			session.close();
			return user;
		
		
	}
		

}
