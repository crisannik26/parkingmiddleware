/**
 * 
 */
package it.unisalento.parking.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import it.unisalento.parking.dao.FactoryDao;
import it.unisalento.parking.dao.FotoDao;
import it.unisalento.parking.domain.Foto;
import it.unisalento.parking.util.HibernateUtil;

/**
 * @author joker
 *
 */
public class FotoDaoImpl extends BaseDaoImpl<Foto> implements FotoDao{
	String tabella=Foto.class.getName();
	@SuppressWarnings("unchecked")
	@Override
	public List<Foto> exctractFoto(int idReport) {
		Session session = HibernateUtil.openSession();
		Transaction tx = session.beginTransaction();
		List<Foto> list =(List<Foto>) session.createQuery("from "+tabella+" where Segnalazione_idSegnalazione=:idreport").setParameter("idreport", idReport).list();
		tx.commit();
		session.close();
		return list;
	}

}
