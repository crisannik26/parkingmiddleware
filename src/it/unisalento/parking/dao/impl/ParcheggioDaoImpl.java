/**
 * 
 */
package it.unisalento.parking.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import it.unisalento.parking.dao.ParcheggioDao;
import it.unisalento.parking.domain.Parcheggio;
import it.unisalento.parking.util.HibernateUtil;
/**
 * @author joker
 *
 */
public class ParcheggioDaoImpl extends BaseDaoImpl<Parcheggio> implements ParcheggioDao{
	
	String tabella=Parcheggio.class.getName();
	@SuppressWarnings("unchecked")
	public List<Parcheggio> infopark(int idzone){
		Session session = HibernateUtil.openSession();
		Transaction tx = session.beginTransaction();
		List<Parcheggio> list_park = (List<Parcheggio>) session.createQuery("from "+tabella+" where Zona_idZona=:id").setParameter("id", idzone).list();
		tx.commit();
		session.close();
			
		return list_park;
	}

}
