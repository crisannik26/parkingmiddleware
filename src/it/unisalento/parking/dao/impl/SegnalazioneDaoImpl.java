/**
 * 
 */
package it.unisalento.parking.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import it.unisalento.parking.dao.SegnalazioneDao;
import it.unisalento.parking.domain.Segnalazione;
import it.unisalento.parking.util.HibernateUtil;

/**
 * @author joker
 *
 */
public class SegnalazioneDaoImpl extends BaseDaoImpl<Segnalazione> implements SegnalazioneDao{
	String tabella= Segnalazione.class.getName();

	@SuppressWarnings("unchecked")
	@Override
	public List<Segnalazione> viewSegn(int idgestore) {
		Session session = HibernateUtil.openSession();
		Transaction tx = session.beginTransaction();
		List<Segnalazione> lista=(List<Segnalazione>) session.createQuery("from "+tabella+" where Gestore_idGestore=:id").setParameter("id", idgestore).list();
		tx.commit();
		session.close();
		return lista;
	}
	
	public Integer numberFalseSegn(int idgestore){
		Session session = HibernateUtil.openSession();
		Transaction tx = session.beginTransaction();
		Long count = (Long) session.createQuery("select count(*) from "+tabella+" where Gestore_idGestore=:id and view=false").setParameter("id", idgestore).uniqueResult();
		int counts= count.intValue();
		tx.commit();
		session.close();
		
		return counts;
	}
	
	public Segnalazione idaftersendreport(){
		
		Session session = HibernateUtil.openSession();
		Transaction tx = session.beginTransaction();
		Segnalazione idsegnalazione =(Segnalazione) session.createQuery("from "+ tabella+" order by idSegnalazione desc").setMaxResults(1).uniqueResult();
		tx.commit();
		session.close();
		return idsegnalazione;
	}
	
	public Segnalazione changeSegn( int idseg, int idgest){
		
		Session session = HibernateUtil.openSession();
		Transaction tx = session.beginTransaction();
		Segnalazione segnalazione = (Segnalazione) session.createQuery("from "+tabella+" where idSegnalazione=:ids and Gestore_idGestore=:idg")
				.setParameter("ids", idseg).setParameter("idg", idgest).uniqueResult();
		tx.commit();
		session.close();
		return segnalazione;
	}
}
