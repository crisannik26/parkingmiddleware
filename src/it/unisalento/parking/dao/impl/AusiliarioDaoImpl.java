/**
 * 
 */
package it.unisalento.parking.dao.impl;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import it.unisalento.parking.dao.AusiliarioDao;
import it.unisalento.parking.domain.Ausiliario;
import it.unisalento.parking.domain.Utente;
import it.unisalento.parking.util.HibernateUtil;
/**
 * @author joker
 *
 */
public class AusiliarioDaoImpl extends BaseDaoImpl<Ausiliario> implements AusiliarioDao {
	String tabella= Ausiliario.class.getName();
	public Ausiliario extractinfo(int id){
		
			Session session = HibernateUtil.openSession();
			Transaction tx =session.beginTransaction();
			Ausiliario ausiliario = (Ausiliario) session.createQuery("from "+tabella+" where Persona_idPersona=:id").setParameter("id", id).uniqueResult();
			tx.commit();
			session.close();
			return ausiliario;
		
		
		
		
	}

}
