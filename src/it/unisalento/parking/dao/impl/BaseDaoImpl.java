package it.unisalento.parking.dao.impl;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;

import it.unisalento.parking.dao.BaseDao;
import it.unisalento.parking.util.HibernateUtil;

public class BaseDaoImpl<T> implements BaseDao<T> {

	/* (non-Javadoc)
	 * @see it.unisalento.parking.dao.BaseDao#save(java.lang.Object)
	 */
	@Override
	public void save(T entity) {
		
		Session session = HibernateUtil.openSession();
		Transaction tx = session.beginTransaction();
		session.save(entity);
		//getTransaction fornisce l'istanza della transazione associata con la sessione
		tx.commit();
		session.close();
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see it.unisalento.parking.dao.BaseDao#getById(int, java.lang.Class)
	 */
	@Override
	public T getById(int id, Class<T> clazz) {
		
		Session session = HibernateUtil.openSession();
		Transaction tx = session.beginTransaction();
		@SuppressWarnings("unchecked") //in compilatore ignora eventuali errori se necessario in fase di warning
		T entity =(T) session.get(clazz.getName(), id);
		tx.commit();
		session.close();
		// TODO Auto-generated method stub
		return entity;
	}

	/* (non-Javadoc)
	 * @see it.unisalento.parking.dao.BaseDao#getByAll(java.lang.Class)
	 */
	@Override
	public List<T> getByAll(Class<T> clazz) {
		
		Session session = HibernateUtil.openSession();
		Transaction tx = session.beginTransaction();
		@SuppressWarnings("unchecked")
		List<T> returnList = session.createQuery("FROM "+ clazz.getName()).list();
		tx.commit();
		session.close();
		// TODO Auto-generated method stub
		return returnList;
	}

	/* (non-Javadoc)
	 * @see it.unisalento.parking.dao.BaseDao#delete(int, java.lang.Class)
	 */
	@Override
	public T delete(int id, Class<T> clazz) {
		
		Session session = HibernateUtil.openSession();
		Transaction tx = session.beginTransaction();
		@SuppressWarnings("unchecked")
		T entity = (T) session.get(clazz.getName(), id);
		session.delete(entity);
		tx.commit();
		session.close();
		
		// TODO Auto-generated method stub
		return entity;
	}

	/* (non-Javadoc)
	 * @see it.unisalento.parking.dao.BaseDao#update(java.lang.Object)
	 */
	@Override
	public void update(T entity) throws Exception {
		
		Session session = HibernateUtil.openSession();
		Transaction tx = session.beginTransaction();
		session.update(entity);
		tx.commit();
		session.close();
		// TODO Auto-generated method stub
		
	}

}
