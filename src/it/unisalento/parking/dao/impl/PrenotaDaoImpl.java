/**
 * 
 */
package it.unisalento.parking.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import it.unisalento.parking.dao.PrenotaDao;
import it.unisalento.parking.domain.Prenota;
import it.unisalento.parking.util.HibernateUtil;
/**
 * @author joker
 *
 */
public class PrenotaDaoImpl extends BaseDaoImpl<Prenota> implements PrenotaDao{
	String tabella = Prenota.class.getName();
	@SuppressWarnings("unchecked")
	public List<Prenota> searchcar(String targa, int idPark){
		//RICERCA MACCHINA IN BASE ALLA TARGA
		Session session = HibernateUtil.openSession();
		Transaction tx = session.beginTransaction();
		List<Prenota> prenota = (List<Prenota>) session.createQuery("from "+ tabella+" where targa=:t").setParameter("t", targa).list();
		tx.commit();
		session.close();
		
		
		List<Prenota> prenotaForIdPark = new ArrayList<Prenota>();
		
		for (Prenota prenotation:prenota){
			if (prenotation.getParcheggio().getZona().getIdZona() == idPark){
				prenotaForIdPark.add(prenotation);
			}
		}
		
		return prenotaForIdPark;
	}
	
//	public Prenota idUserforViolation(String targa){
//		//NECESSARIO PER ESEGUIRE LA CONTRAVVENZIONE, IN QUANTO VADO A SELEZIONARE L'ID UTENTE IN BASE ALLA TARGA
//		Session session = HibernateUtil.openSession();
//		Transaction tx = session.beginTransaction();
//		Prenota iduser = (Prenota) session.createQuery("from "+tabella+" where targa=:t").setParameter("t", targa).uniqueResult();
//		tx.commit();
//		session.close();
//		return iduser;
//	}
	
	public Prenota infoprenotazione(int idpark){
		Session session = HibernateUtil.openSession();
		Transaction tx = session.beginTransaction();
		Prenota prenota = (Prenota) session.createQuery("from "+tabella+" where Parcheggio_idParcheggio=:id").setParameter("id", idpark).uniqueResult();
		
		tx.commit();
		session.close();
		return prenota;
	}
	
	@SuppressWarnings("unchecked")
	public List<Prenota> allinforinfunctionuser(int iduser){
		Session session = HibernateUtil.openSession();
		Transaction tx = session.beginTransaction();
		List<Prenota> prenota = (List<Prenota>) session.createQuery("from "+tabella+" where Utente_idUtente=:id").setParameter("id", iduser).list();
		tx.commit();
		session.close();
		
		return prenota;
	}

}
