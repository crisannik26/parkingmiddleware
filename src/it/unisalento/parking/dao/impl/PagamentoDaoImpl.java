/**
 * 
 */
package it.unisalento.parking.dao.impl;

import java.util.List;
import it.unisalento.parking.dao.PagamentoDao;
import it.unisalento.parking.domain.Pagamento;

/**
 * @author joker
 *
 */
public class PagamentoDaoImpl extends BaseDaoImpl<Pagamento> implements PagamentoDao{

}
