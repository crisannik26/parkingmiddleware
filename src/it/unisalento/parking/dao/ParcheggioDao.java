package it.unisalento.parking.dao;

import java.util.List;

import it.unisalento.parking.domain.Parcheggio;

public interface ParcheggioDao extends BaseDao<Parcheggio> {
	
	public List<Parcheggio> infopark(int idzone);

}
