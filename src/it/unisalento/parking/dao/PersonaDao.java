package it.unisalento.parking.dao;

import java.util.List;

import it.unisalento.parking.domain.Persona;

public interface PersonaDao extends BaseDao<Persona> {
	
	public abstract Persona login(String mail);
	public abstract Persona getid(String mail);
}
