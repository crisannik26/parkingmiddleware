package it.unisalento.parking.dao;

import it.unisalento.parking.domain.Ausiliario;
import it.unisalento.parking.domain.Gestore;

public interface GestoreDao extends BaseDao<Gestore> {
	public abstract Gestore extractinfo(int id);

	
}
