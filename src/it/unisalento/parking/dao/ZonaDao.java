package it.unisalento.parking.dao;

import java.util.List;

import it.unisalento.parking.domain.Zona;

public interface ZonaDao extends BaseDao<Zona> {
	
	public List<Zona> extractinfo(int idgestore);
	public Zona idaftercreate();

}
