/**
 * 
 */
package it.unisalento.parking.dao;

import it.unisalento.parking.dao.impl.AusiliarioDaoImpl;
import it.unisalento.parking.dao.impl.CartaCreditoDaoImpl;
import it.unisalento.parking.dao.impl.FotoDaoImpl;
import it.unisalento.parking.dao.impl.GestoreDaoImpl;
import it.unisalento.parking.dao.impl.MultaDaoImpl;
import it.unisalento.parking.dao.impl.PagamentoDaoImpl;
import it.unisalento.parking.dao.impl.ParcheggioDaoImpl;
import it.unisalento.parking.dao.impl.PersonaDaoImpl;
import it.unisalento.parking.dao.impl.PrenotaDaoImpl;
import it.unisalento.parking.dao.impl.SegnalaDaoImpl;
import it.unisalento.parking.dao.impl.SegnalazioneDaoImpl;
import it.unisalento.parking.dao.impl.TipologiaParcheggioDaoImpl;
import it.unisalento.parking.dao.impl.UtenteDaoImpl;
import it.unisalento.parking.dao.impl.ZonaDaoImpl;
/**
 * @author joker
 *
 */
//use Singleton (Creationaly pattern)
public class FactoryDao {
	
	private static FactoryDao factory;
	
	private FactoryDao(){
		
	}
	public static FactoryDao getInstance(){
		if (factory == null){
			factory = new FactoryDao();
		}
		return factory;	
	}
	
	public PersonaDao getPersonaDao(){
		return new PersonaDaoImpl();
	}
	
	public UtenteDao getUtenteDao(){
		return new UtenteDaoImpl();
	}
	
	public AusiliarioDao getAusiliarioDao(){
		return new AusiliarioDaoImpl();
	}
	
	public GestoreDao getGestoreDao(){
		return new GestoreDaoImpl();
	}
	public SegnalazioneDao getSegnalazioneDao(){
		return new SegnalazioneDaoImpl();
	}
	public ZonaDao getZonaDao() {
		return new ZonaDaoImpl();
		
	}
	public TipologiaParcheggioDao getTipologiaParcheggioDao(){
		return new TipologiaParcheggioDaoImpl();
	}
	public CartaCreditoDao getCartaCreditoDao(){
		return new CartaCreditoDaoImpl();
	}
	public SegnalaDao getSegnalaDao(){
		return new SegnalaDaoImpl();
	}
	public FotoDao getFotoDao(){
		return new FotoDaoImpl();
	}
	public PrenotaDao getPrenotaDao(){
		return new PrenotaDaoImpl();
	}
	public MultaDao getMultaDao(){
		return new MultaDaoImpl();
	}
	public PagamentoDao getPagamentoDao(){
		return new PagamentoDaoImpl();
	}
	public ParcheggioDao getParcheggioDao(){
		return new ParcheggioDaoImpl();
	}

}
