package it.unisalento.parking.dao;

import java.util.List;

import it.unisalento.parking.domain.Multa;

public interface MultaDao extends BaseDao<Multa> {
	
	public List<Multa> getMulteByIdAus(int idaus);
	
	public List<Multa> getMulteByIdUser(int iduser);

}
