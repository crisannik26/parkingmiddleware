package it.unisalento.parking.dao;

import java.util.List;

import it.unisalento.parking.domain.TipologiaParcheggio;

public interface TipologiaParcheggioDao extends BaseDao<TipologiaParcheggio>{
	public List<TipologiaParcheggio> extractinfo(int idzone);
	public int NumberPlace(int idzone);
	public TipologiaParcheggio extractinfotype(int idzone, int idtype);
}
