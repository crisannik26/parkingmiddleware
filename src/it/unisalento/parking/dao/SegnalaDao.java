package it.unisalento.parking.dao;

import it.unisalento.parking.domain.Segnala;

public interface SegnalaDao extends BaseDao<Segnala> {
	public Segnala extractinfo(int idsegnalazione);

}
