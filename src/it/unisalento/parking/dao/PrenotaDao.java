package it.unisalento.parking.dao;

import java.util.List;

import it.unisalento.parking.domain.Prenota;

public interface PrenotaDao extends BaseDao<Prenota> {
	public List<Prenota> searchcar(String targa, int idPark);
//	public Prenota idUserforViolation(String targa);
	public Prenota infoprenotazione(int idpark);
	public List<Prenota> allinforinfunctionuser(int iduser);

}
