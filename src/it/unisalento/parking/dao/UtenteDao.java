package it.unisalento.parking.dao;

import java.util.List;

import it.unisalento.parking.domain.Utente;

public interface UtenteDao extends BaseDao<Utente>{
//	public List<Utente> logAsUser(String mail, String pass);
	public abstract Utente extractinfo(int idpersona);
	

}
