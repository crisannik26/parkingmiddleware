package it.unisalento.parking.dao;

import it.unisalento.parking.domain.Pagamento;

public interface PagamentoDao extends BaseDao<Pagamento>{

}
