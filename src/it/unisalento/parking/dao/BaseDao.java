package it.unisalento.parking.dao;

import java.util.List;

//CRUD operations

public interface BaseDao<T> {
	
	void save(T entity);
	
	T getById(int id, Class<T> clazz);
	
	List<T> getByAll(Class<T> clazz);
	
	T delete(int id, Class<T> clazz);
	
	void update(T entity) throws Exception;

}
