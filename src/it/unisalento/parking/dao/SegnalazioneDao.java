package it.unisalento.parking.dao;

import java.util.List;

import it.unisalento.parking.domain.Segnalazione;

public interface SegnalazioneDao extends BaseDao<Segnalazione> {
	
	public List<Segnalazione> viewSegn(int idgestore);
	public Integer numberFalseSegn(int idgestore);
	public Segnalazione idaftersendreport();
	public Segnalazione changeSegn( int idseg, int idgest);

}
