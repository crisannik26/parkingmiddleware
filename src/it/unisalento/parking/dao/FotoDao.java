package it.unisalento.parking.dao;

import java.util.List;

import it.unisalento.parking.domain.Foto;

public interface FotoDao extends BaseDao<Foto>{

	List<Foto> exctractFoto(int idReport);
	
}
