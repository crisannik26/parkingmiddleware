package it.unisalento.parking.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class HibernateUtil {
	private static SessionFactory sessionFactory;
	
	static{
		Configuration conf = new Configuration();
		conf.configure(); //usa il mapping e le proprieta specificate nel file hibernate.cfg.xml
		
		StandardServiceRegistryBuilder serviceRegBuilder = new StandardServiceRegistryBuilder();
		serviceRegBuilder.applySettings(conf.getProperties()); //si applica un gruppo di valori di setting, passati dal conf.getProp
		
		ServiceRegistry serviceRegistry = serviceRegBuilder.build(); //.build e necessario per costruire un istanza di ServiceRegistry
		sessionFactory = conf.buildSessionFactory(serviceRegistry);
		
		sessionFactory = new Configuration().configure().buildSessionFactory();
			
	}
	
	public static SessionFactory getSessionFactory(){
		return sessionFactory;
		
	}
	
	
	public static Session openSession(){
		//Session rappresenta l'interfaccia di esecuzione principale tra un applicazione Java e Hibernate
		return sessionFactory.openSession();
		
	}

}
