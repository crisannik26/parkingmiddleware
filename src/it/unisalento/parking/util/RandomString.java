package it.unisalento.parking.util;

import java.util.Random;

public class RandomString {
	
	public static String randomGenerator(){
		char[] chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
		StringBuilder sb = new StringBuilder();
		Random random = new Random();
		for (int i = 0; i < 12; i++) {
		    char c = chars[random.nextInt(chars.length)];
		    sb.append(c);
		}
		String randomString= sb.toString();
		return randomString;
	}

}
