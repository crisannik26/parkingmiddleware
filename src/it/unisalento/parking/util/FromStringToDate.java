package it.unisalento.parking.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FromStringToDate{
	public static Date convert(String date) throws Exception{
		
		DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss"); 
		Date startDate;
		startDate = df.parse(date);
		return startDate;
	}

}

