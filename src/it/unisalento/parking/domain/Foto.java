package it.unisalento.parking.domain;
// Generated 30-giu-2017 15.13.52 by Hibernate Tools 5.2.3.Final

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Foto generated by hbm2java
 */
@Entity
@Table(name = "Foto", catalog = "parkingdb")
public class Foto implements java.io.Serializable {

	private Integer idFoto;
	private Segnalazione segnalazione;
	private String fotografia;

	public Foto() {
	}

	public Foto(Segnalazione segnalazione, String fotografia) {
		this.segnalazione = segnalazione;
		this.fotografia = fotografia;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "idFoto", unique = true, nullable = false)
	public Integer getIdFoto() {
		return this.idFoto;
	}

	public void setIdFoto(Integer idFoto) {
		this.idFoto = idFoto;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Segnalazione_idSegnalazione", nullable = false)
	public Segnalazione getSegnalazione() {
		return this.segnalazione;
	}

	public void setSegnalazione(Segnalazione segnalazione) {
		this.segnalazione = segnalazione;
	}

	@Column(name = "fotografia", nullable = false, length = 2000)
	public String getFotografia() {
		return this.fotografia;
	}

	public void setFotografia(String fotografia) {
		this.fotografia = fotografia;
	}

}
