package it.unisalento.parking.domain.test;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import it.unisalento.parking.dao.FactoryDao;
import it.unisalento.parking.domain.Ausiliario;
import it.unisalento.parking.domain.Multa;
import it.unisalento.parking.domain.Utente;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MultaDaoTest {
	static Ausiliario policeman;
	static Utente user;
	static Multa multa;
	static Multa multaOther;
	static Date date;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		multa = new Multa();
		policeman = new Ausiliario();
		user = new Utente();
		date = new Date();

		policeman.setIdAusiliario(1);
		user.setIdUtente(1);
		multaOther = new Multa(policeman, user, date, 5);
		multaOther = new Multa(policeman, user, date, 5, "TEST");
	}

	

	@Before
	public void setUp() throws Exception {
		
	}

	@Test
	public void createMulta_Test(){
		multa.setCausale("TEST");
		multa.setDataMulta(date);
		multa.setImporto((float) 52.23);
		multa.setAusiliario(policeman);
		multa.setUtente(user);
		FactoryDao.getInstance().getMultaDao().save(multa);
		assertNotNull(multa);
	}

	@Test
	public void getMulteByIdAusWithIdPoliceman_Test() {
		
		List<Multa> multaList = FactoryDao.getInstance().getMultaDao().getMulteByIdAus(policeman.getIdAusiliario());
		assertNotNull(multaList.size());
		
	}
	
	@Test
	public void getMulteByIdUserWithIdUser_Test() {
		
		List<Multa> multaList = FactoryDao.getInstance().getMultaDao().getMulteByIdUser(user.getIdUtente());
		assertNotNull(multaList.size());
		
	}
	
	@Test
	public void getByIdMulta_Test(){
		assertNotNull(FactoryDao.getInstance().getMultaDao().getById(multa.getCodiceMulta(), Multa.class));
	}
	
	@Test
	public void getAllMulta_Test(){
		List<Multa> multaList=(List<Multa>)FactoryDao.getInstance().getMultaDao().getByAll(Multa.class);
		assertNotNull(multaList.size());
	}
	
	@Test
	public void updateMulta_Test() throws Exception{
		multa.setImporto(10);
		multa.setCausale("Update Description");
		FactoryDao.getInstance().getMultaDao().update(multa);
		assertNotNull(multa);
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		if(multa.getCodiceMulta()!=0){
			FactoryDao.getInstance().getMultaDao().delete(multa.getCodiceMulta(), Multa.class);
		}
	}
	
	@After
	public void tearDown() throws Exception {
	}

}
