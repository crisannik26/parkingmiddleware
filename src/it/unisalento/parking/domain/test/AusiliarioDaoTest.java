package it.unisalento.parking.domain.test;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Random;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import it.unisalento.parking.dao.FactoryDao;
import it.unisalento.parking.domain.Ausiliario;
import it.unisalento.parking.domain.Persona;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AusiliarioDaoTest {
	
	static Integer idperson;
	static Persona persona;
	static Ausiliario policeman;
	static Ausiliario policemanOther;
	static Random rand;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		idperson=3;
		persona = new Persona();
		persona.setIdPersona(idperson);
		policeman=new Ausiliario();
		policemanOther = new Ausiliario(persona, "example");
		rand = new Random();

	}



	@Before
	public void setUp() throws Exception {
		
	}

	
	@Test
	public void extractInfoPoliceman_Test() {
		policeman=FactoryDao.getInstance().getAusiliarioDao().extractinfo(idperson);
		assertNotNull(policeman);
	}
	
	@Test
	public void getByAllPoliceman_Test(){
		List<Ausiliario> policemanList = (List<Ausiliario>) FactoryDao.getInstance().getAusiliarioDao().getByAll(Ausiliario.class);
		assertNotNull(policemanList.size());
	}
	
	@Test
	public void getIdPoliceman_Test(){
		assertNotNull(FactoryDao.getInstance().getAusiliarioDao().getById(policeman.getIdAusiliario(), Ausiliario.class));
	}
	
	@Test
	public void updatePoliceman_Test() throws Exception{
		policeman.setCodiceAus("Test"+(rand.nextInt(50)+1));
		FactoryDao.getInstance().getAusiliarioDao().update(policeman);
		assertNotNull(policeman);
		
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		policemanOther=null;
	}
	
	@After
	public void tearDown() throws Exception {
	}


}
