package it.unisalento.parking.domain.test;

import static org.junit.Assert.*;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import it.unisalento.parking.dao.FactoryDao;
import it.unisalento.parking.domain.Gestore;
import it.unisalento.parking.domain.Segnala;
import it.unisalento.parking.domain.Segnalazione;
import it.unisalento.parking.domain.Utente;
import it.unisalento.parking.domain.Zona;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SegnalaDaoTest {
	
	static Segnala segnala;
	static Segnalazione segnalazione;
	static Zona zona;
	static Utente user;
	static Gestore gestore;
	static Date date;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		segnala=new Segnala();
		segnalazione= new Segnalazione();
		zona = new Zona();
		user = new Utente();
		gestore = new Gestore();
		date = new Date();
		
		
		zona.setIdZona(2);
		user.setIdUtente(1);
		gestore.setIdGestore(1);
		
		segnalazione.setDescrizione("Test");
		segnalazione.setGestore(gestore);
		segnalazione.setTitolo("TestTitle");
		segnalazione.setView(false);
		
		FactoryDao.getInstance().getSegnalazioneDao().save(segnalazione);
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void createSegnala_Test(){
		segnala.setSegnalazione(segnalazione);
		segnala.setUtente(user);
		segnala.setZona(zona);
		segnala.setDataSegnalazione(date);
		
		FactoryDao.getInstance().getSegnalaDao().save(segnala);
		assertNotNull(segnala);
	}
	
	@Test
	public void getByIdSegnala_Test(){
		assertNotNull(FactoryDao.getInstance().getSegnalaDao().getById(segnala.getIdSegnala(), Segnala.class));
	}
	
	@Test
	public void getAllSegnala_Test(){
		List<Segnala> segnalaList = (List<Segnala>)FactoryDao.getInstance().getSegnalaDao().getByAll(Segnala.class);
		assertNotNull(segnalaList.size());
	}
		
	@Test
	public void getInfoSegnalaWithIdSegnalazione_Test() {
		
		assertNotNull(FactoryDao.getInstance().getSegnalaDao().extractinfo(segnalazione.getIdSegnalazione()));
	}
	
	@Test
	public void updateSegnala_Test() throws Exception{
		Calendar updateDate = Calendar.getInstance();
		updateDate.add(Calendar.MONTH, 1);
		date=updateDate.getTime();
		
		segnala.setDataSegnalazione(date);
		FactoryDao.getInstance().getSegnalaDao().update(segnala);
		assertNotNull(segnala);
		
		
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		if((segnala.getIdSegnala()!=0)&&(segnalazione.getIdSegnalazione()!=0)){
			FactoryDao.getInstance().getSegnalaDao().delete(segnala.getIdSegnala(), Segnala.class);
			FactoryDao.getInstance().getSegnalazioneDao().delete(segnalazione.getIdSegnalazione(), Segnalazione.class);
		}			
	}

}
