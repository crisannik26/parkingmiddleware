package it.unisalento.parking.domain.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import it.unisalento.parking.dao.BaseDao;
import it.unisalento.parking.dao.impl.BaseDaoImpl;
import it.unisalento.parking.domain.Persona;

public class BaseDaoTest {
	BaseDaoImpl<Persona> test;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		test = new BaseDaoImpl<Persona>();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		assertTrue(test.getByAll(Persona.class).size()>0);
	}

}
