package it.unisalento.parking.domain.test;

import static org.junit.Assert.*;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import it.unisalento.parking.dao.FactoryDao;
import it.unisalento.parking.domain.Pagamento;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PagamentoDaoTest {
	
	static Pagamento pay;
	static Pagamento payOther;
	static Date date;
	static Calendar dateFired;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		pay = new Pagamento();
		date = new Date();
		payOther=new Pagamento(5, date, "numeroCarta", "circuito", "data", "nome", "cognome");

					
	}
	
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void createPagamento_Test(){
		pay.setCircuito("Test");
		pay.setCognomeProprietario("CognomeTest");
		pay.setImporto(50);
		pay.setNomeProprietario("NomeTest");
		pay.setNumeroCarta("NumeroTest");
		pay.setDataPagamento(date);
		pay.setDataScadenza("testd");
		
		FactoryDao.getInstance().getPagamentoDao().save(pay);
		assertNotNull(pay);
	}
	
	@Test
	public void getByIdPagamento_Test(){
		assertNotNull(FactoryDao.getInstance().getPagamentoDao().getById(pay.getIdPagamento(), Pagamento.class));
	}
	
	@Test
	public void getAllPagamento_Test(){
		List<Pagamento> payList = (List<Pagamento>)FactoryDao.getInstance().getPagamentoDao().getByAll(Pagamento.class);
	}
	
	@Test
	public void updatePagamento_Test() throws Exception{
		pay.setCircuito("CIRCUITO");
		pay.setDataScadenza("SCAD");
		
		FactoryDao.getInstance().getPagamentoDao().update(pay);
		assertNotNull(pay);
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		if(pay.getIdPagamento()!=0){
			FactoryDao.getInstance().getPagamentoDao().delete(pay.getIdPagamento(), Pagamento.class);
		}
		payOther=null;
	}

	@After
	public void tearDown() throws Exception {
	}

}
