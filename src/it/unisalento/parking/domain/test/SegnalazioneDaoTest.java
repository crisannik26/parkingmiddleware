package it.unisalento.parking.domain.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import it.unisalento.parking.dao.FactoryDao;
import it.unisalento.parking.domain.Gestore;
import it.unisalento.parking.domain.Segnalazione;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SegnalazioneDaoTest {
	
	static Integer count;
	static Gestore gestore;
	static Segnalazione segnalazione;
	static Segnalazione segnalazioneOther;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		gestore=new Gestore();
		gestore.setIdGestore(1);
		segnalazione=new Segnalazione();
		segnalazioneOther=new Segnalazione(gestore, "TITOLO", "DESCRIZIONE");
		count=0;
	}

	

	@Before
	public void setUp() throws Exception {
		
	}

	@Test
	public void viewSegnSegnalazioneWithIdGestore_Test() {
		
		List<Segnalazione> list = FactoryDao.getInstance().getSegnalazioneDao().viewSegn(gestore.getIdGestore());
		assertNotNull(list.size());
	}
	
	@Test
	public void numberFalseSegnSegnalazioneWithIdGestore_Test(){
		count = FactoryDao.getInstance().getSegnalazioneDao().numberFalseSegn(gestore.getIdGestore());
		assertTrue("Count is greater than 0", count>0);
		
	}
	
	@Test
	public void idAfterSendReportSegnalazione_Test(){
		
		assertNotNull(FactoryDao.getInstance().getSegnalazioneDao().idaftersendreport());
	}
	
	@Test
	public void updateSegnSegnalazione_Test(){
		assertNotNull(FactoryDao.getInstance().getSegnalazioneDao().changeSegn(segnalazione.getIdSegnalazione(), gestore.getIdGestore()));
	}
	
	@Test
	public void createSegnalazione_Test(){
		segnalazione.setDescrizione("Descrizione Prova");
		segnalazione.setGestore(gestore);
		segnalazione.setTitolo("Titolo Prova");
		segnalazione.setView(false);
		
		FactoryDao.getInstance().getSegnalazioneDao().save(segnalazione);
		assertNotNull(segnalazione);
	}
	
	@Test
	public void getByIdSegnalazione_Test(){
		assertNotNull(FactoryDao.getInstance().getSegnalazioneDao().getById(segnalazione.getIdSegnalazione(), Segnalazione.class));
	}
	
	@Test
	public void getAllSegnalazione_Test(){
		List<Segnalazione> segnalazioneList=(List<Segnalazione>)FactoryDao.getInstance().getSegnalazioneDao().getByAll(Segnalazione.class);
		assertNotNull(segnalazioneList.size());
	}
	
	@Test
	public void updateSegnalazione_Test() throws Exception{
		segnalazione.setDescrizione("Nuova Descrizione Prova");
		segnalazione.setTitolo("Titolo nuovo");
		
		FactoryDao.getInstance().getSegnalazioneDao().update(segnalazione);
		
		assertNotNull(segnalazione);
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		if(segnalazione.getIdSegnalazione()!=0){
			FactoryDao.getInstance().getSegnalazioneDao().delete(segnalazione.getIdSegnalazione(), Segnalazione.class);
		}
		segnalazioneOther=null;
	}
	
	@After
	public void tearDown() throws Exception {
	}

}
