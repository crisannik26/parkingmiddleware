package it.unisalento.parking.domain.test;

import it.unisalento.parking.util.DistanceGPSUtil;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class DistanceGPSUtilTest {
	
	
	double lat_start;
	double lat_end;
	double lon_start;
	double lon_end;
	double distance;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		lat_start=25.2;
		lat_end=27.3;
		lon_start=31.2;
		lon_end=34.5;
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void distanceGps_Test() {
		
		distance=DistanceGPSUtil.distance(lat_start, lat_end, lon_start, lon_end);
		assertNotNull(distance);
	}

}
