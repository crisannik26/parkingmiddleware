package it.unisalento.parking.domain.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import it.unisalento.parking.dao.FactoryDao;
import it.unisalento.parking.domain.Gestore;
import it.unisalento.parking.domain.Persona;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GestoreDaoTest {
	
	static Integer idperson;
	static Persona persona;
	static Gestore gestore;
	static Gestore gestoreOther;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		gestore = new Gestore();
		idperson=2;
		persona=new Persona();
		persona.setIdPersona(idperson);
		gestoreOther=new Gestore(persona);

	}
	
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void extractInfoGestore_Test() {
		gestore = FactoryDao.getInstance().getGestoreDao().extractinfo(idperson);
		assertNotNull(gestore);
	}
	
	@Test
	public void getByIdGestore_Test(){
		assertNotNull(FactoryDao.getInstance().getGestoreDao().getById(gestore.getIdGestore(), Gestore.class));
	}
	
	@Test
	public void getAllGestore_Test(){
		List<Gestore> gestoreList = (List<Gestore>)FactoryDao.getInstance().getGestoreDao().getByAll(Gestore.class);
		assertNotNull(gestoreList.size());
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		gestoreOther=null;
	}

	@After
	public void tearDown() throws Exception {
	}

}
