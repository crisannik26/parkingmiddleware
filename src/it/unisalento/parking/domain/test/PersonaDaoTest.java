package it.unisalento.parking.domain.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.junit.FixMethodOrder;


import it.unisalento.parking.dao.FactoryDao;
import it.unisalento.parking.dao.PersonaDao;
import it.unisalento.parking.dao.impl.PersonaDaoImpl;
import it.unisalento.parking.domain.Persona;
import it.unisalento.parking.util.PasswordUtil;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PersonaDaoTest {
	
	static Persona persona;
	static Persona personaOther;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		persona=new Persona();
		personaOther=new Persona("Test", "Test", "Test", PasswordUtil.getSaltedHash("Test"), "Test");
		
		persona.setNome("nome");
		persona.setCognome("cognome");
		persona.setMail("mailOfTest");
		persona.setPassword(PasswordUtil.getSaltedHash("password"));
		persona.setTelefono("telefono");
	}

	@Before
	public void setUp() throws Exception {
	
	}
	
	@Test
	public void createPerson_Test() throws Exception{
		FactoryDao.getInstance().getPersonaDao().save(persona);
		assertNotNull(persona);
	}
		
	@Test 
	public void loginPerson_Test() {
		assertNotNull(FactoryDao.getInstance().getPersonaDao().login(persona.getMail()));
	}
	
	@Test
	public void getByIdPerson_Test(){
		assertNotNull(FactoryDao.getInstance().getPersonaDao().getById(persona.getIdPersona(), Persona.class));
	}
	
	@Test
	public void getAll_person_test(){
		List<Persona> list = FactoryDao.getInstance().getPersonaDao().getByAll(Persona.class);
		assertNotNull(list.size());
	}

	@Test
	public void updatePerson_Test() throws Exception{
		persona.setTelefono("tel");
		persona.setCognome("Cotardo");
		persona.setNome("Angelo");
		persona.setMail("acTestMail@gmail.com");
		persona.setPassword(PasswordUtil.getSaltedHash("angelo"));
		
		FactoryDao.getInstance().getPersonaDao().update(persona);
		assertNotNull(persona);
		
	}
	
	@Test
	public void getIdPerson(){
		assertNotNull(FactoryDao.getInstance().getPersonaDao().getid(persona.getMail()));
	}
	@After
	public void tearDown() throws Exception {
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		if(persona.getIdPersona()!=0){
			FactoryDao.getInstance().getPersonaDao().delete(persona.getIdPersona(), Persona.class);
		}
	}

}
