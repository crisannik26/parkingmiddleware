package it.unisalento.parking.domain.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import it.unisalento.parking.dao.FactoryDao;
import it.unisalento.parking.domain.Foto;
import it.unisalento.parking.domain.Gestore;
import it.unisalento.parking.domain.Segnalazione;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class FotoDaoTest {
	static Segnalazione segnalazione;
	static Gestore gestore;
	static Foto foto;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		segnalazione = new Segnalazione();
		foto = new Foto();
		segnalazione.setDescrizione("Prova");
		segnalazione.setTitolo("Prova");
		segnalazione.setView(false);
		gestore = new Gestore();
		gestore.setIdGestore(1);
		segnalazione.setGestore(gestore);

		FactoryDao.getInstance().getSegnalazioneDao().save(segnalazione);		
	}

	

	@Before
	public void setUp() throws Exception {
	}

	
	@Test
	public void createPhoto_Test(){
		
		foto.setSegnalazione(segnalazione);
		foto.setFotografia("qwertyuiop");
		FactoryDao.getInstance().getFotoDao().save(foto);
		assertNotNull(foto);
	}
	
	@Test
	public void getByIdPhoto_Test() {
		assertNotNull(FactoryDao.getInstance().getFotoDao().getById(foto.getIdFoto(), Foto.class));
	}
	
	@Test
	public void getByAllPhoto_Test(){
		List<Foto> listFoto =(List<Foto>) FactoryDao.getInstance().getFotoDao().getByAll(Foto.class);
		assertNotNull(listFoto.size());
	}
	
	@Test
	public void updatePhoto_Test() throws Exception{
		foto.setFotografia("NewFoto");
		FactoryDao.getInstance().getFotoDao().update(foto);
		assertNotNull(foto);
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		if((foto.getIdFoto()!=0)&&(segnalazione.getIdSegnalazione()!=0)){
			FactoryDao.getInstance().getFotoDao().delete(foto.getIdFoto(), Foto.class);
			FactoryDao.getInstance().getSegnalazioneDao().delete(segnalazione.getIdSegnalazione(), Segnalazione.class);
		}
	}
	
	@After
	public void tearDown() throws Exception {
	}

}
