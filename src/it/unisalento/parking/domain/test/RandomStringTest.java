package it.unisalento.parking.domain.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import it.unisalento.parking.util.RandomString;

public class RandomStringTest {
	
	static String rndString;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		rndString=null;
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void randomGenerator_Test() {
		rndString=RandomString.randomGenerator();
		assertNotNull(rndString);
	}

}
