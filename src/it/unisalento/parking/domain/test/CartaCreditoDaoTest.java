package it.unisalento.parking.domain.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import it.unisalento.parking.dao.FactoryDao;
import it.unisalento.parking.domain.CartaCredito;
import it.unisalento.parking.domain.Utente;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CartaCreditoDaoTest {
	static Integer idutente;
	static CartaCredito card;
	static int cardDelete;
	static Utente user;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		idutente=2;
		user =new Utente();
		card = new CartaCredito();
		user.setIdUtente(idutente);
		card.setCircuito("circuito");
		card.setCognomeProp("cognome");
		card.setDataScadenza("00/0000");
		card.setNomeProprietario("nome");
		card.setNumeroCarta("numerocarta");
		card.setUtente(user);
	}

	@Before
	public void setUp() throws Exception {
	
		
	}
	@Test
	public void createCard_Test() throws Exception{
		FactoryDao.getInstance().getCartaCreditoDao().save(card);
		cardDelete=card.getIdCarta();
		assertNotNull(card);
		
	}
	
	@Test
	public void getByAllCard_Test() {
		
		List<CartaCredito> list = FactoryDao.getInstance().getCartaCreditoDao().allcard(idutente);
		assertNotNull(list.size());
	}
	
	
	
	@Test
	public void getByIdCard_Test(){
		assertNotNull(FactoryDao.getInstance().getCartaCreditoDao().getById(cardDelete, CartaCredito.class));
		
	}
	
	
	
	@Test
	public void updateCard_Test() throws Exception{
		
		card.setIdCarta(cardDelete);
		card.setDataScadenza("01/1000");
		FactoryDao.getInstance().getCartaCreditoDao().update(card);
		assertNotNull(card);
		
		
	}
	
	@After
	public void tearDown() throws Exception {
		
	}
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		if(cardDelete!=0){
			FactoryDao.getInstance().getCartaCreditoDao().delete(cardDelete, CartaCredito.class);
		}
	}
	
}
