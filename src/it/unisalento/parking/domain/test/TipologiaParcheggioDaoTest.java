package it.unisalento.parking.domain.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import it.unisalento.parking.dao.FactoryDao;
import it.unisalento.parking.domain.TipologiaParcheggio;
import it.unisalento.parking.domain.Zona;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TipologiaParcheggioDaoTest {
	
	static Zona zona;
	static TipologiaParcheggio typePark;
	static Integer tot;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		zona = new Zona();
		typePark = new TipologiaParcheggio();
		tot=0;
		zona.setIdZona(2);
		
	}

	
	@Before
	public void setUp() throws Exception {
		
	}

	@Test
	public void extractInfoTypeParkWithIdZone_Test() {
		
		List<TipologiaParcheggio> typeParkWithIdZoneList = FactoryDao.getInstance().getTipologiaParcheggioDao().extractinfo(zona.getIdZona());
		assertNotNull(typeParkWithIdZoneList.size());
	}
	
	@Test
	public void numberPlaceTypepark_Test(){
		tot = FactoryDao.getInstance().getTipologiaParcheggioDao().NumberPlace(zona.getIdZona());
		assertTrue("There are number place", tot > 0);
	}
	
	@Test
	public void extractInfoTypeWithIdZoneAndIdType_Test(){
		assertNotNull(FactoryDao.getInstance().getTipologiaParcheggioDao().extractinfotype(zona.getIdZona(), typePark.getIdTipologiaParcheggio()));
	}
	
	@Test
	public void createTypeZone(){
		typePark.setZona(zona);
		typePark.setNumeroPosti(1);
		typePark.setCostoMensile(10);
		typePark.setCostoOrario(10);
		typePark.setTipo("auto");
		
		FactoryDao.getInstance().getTipologiaParcheggioDao().save(typePark);
		assertNotNull(typePark);
	}
	
	@Test
	public void getByIdType_Test(){
		assertNotNull(FactoryDao.getInstance().getTipologiaParcheggioDao().getById(typePark.getIdTipologiaParcheggio(), TipologiaParcheggio.class));
	}
	
	@Test
	public void getAllType_Test(){
		List<TipologiaParcheggio> typeparkList=(List<TipologiaParcheggio>)FactoryDao.getInstance().getTipologiaParcheggioDao().getByAll(TipologiaParcheggio.class);
		assertNotNull(typeparkList.size());
	}
	
	@Test
	public void updateType_Test() throws Exception{
		typePark.setCostoMensile(100);
		typePark.setCostoOrario(20);
		typePark.setTipo("moto");
		
		FactoryDao.getInstance().getTipologiaParcheggioDao().update(typePark);
		assertNotNull(typePark);
		
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		if(typePark.getIdTipologiaParcheggio()!=0){
			FactoryDao.getInstance().getTipologiaParcheggioDao().delete(typePark.getIdTipologiaParcheggio(), TipologiaParcheggio.class);
		}
	}
	
	@After
	public void tearDown() throws Exception {
	}


}
