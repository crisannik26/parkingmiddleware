package it.unisalento.parking.domain.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ AusiliarioDaoTest.class, BaseDaoTest.class, CartaCreditoDaoTest.class, DistanceGPSUtilTest.class,
		FotoDaoTest.class, FromStringToDateTest.class, GestoreDaoTest.class, MultaDaoTest.class, PagamentoDaoTest.class,
		ParcheggioDaoTest.class, PersonaDaoTest.class, PrenotaDaoTest.class, RandomStringTest.class, SegnalaDaoTest.class,
		SegnalazioneDaoTest.class, TipologiaParcheggioDaoTest.class, UtenteDaoTest.class, ZonaDaoTest.class })
public class AllTestDatabaseMiddleware {

}
