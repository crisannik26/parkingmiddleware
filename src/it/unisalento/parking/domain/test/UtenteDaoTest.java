package it.unisalento.parking.domain.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import it.unisalento.parking.dao.FactoryDao;
import it.unisalento.parking.domain.Persona;
import it.unisalento.parking.domain.Utente;
import it.unisalento.parking.util.PasswordUtil;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UtenteDaoTest {
	
	static Utente user;
	static Utente userOther;
	static Persona persona;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		user = new Utente();
		userOther = new Utente(persona, "example", "example");
		persona = new Persona();
		persona.setNome("Prova");
		persona.setCognome("Test");
		persona.setMail("mail@mail.com");
		persona.setPassword(PasswordUtil.getSaltedHash("password"));
		
		FactoryDao.getInstance().getPersonaDao().save(persona);
	}

	
	@Before
	public void setUp() throws Exception {
	}

	

	@Test
	public void extractInfoUserWithIdPerson_Test() {
		assertNotNull(FactoryDao.getInstance().getUtenteDao().extractinfo(persona.getIdPersona()));
	}
	
	@Test
	public void createUser_Test(){
		user.setPersona(persona);
		user.setCitta("CittàTest");
		user.setIndirizzo("IndirizzoTest");
		
		FactoryDao.getInstance().getUtenteDao().save(user);
		assertNotNull(user);
	}
	
	@Test
	public void getByIdUser_Test(){
		assertNotNull(FactoryDao.getInstance().getUtenteDao().getById(user.getIdUtente(), Utente.class));
	}
	
	@Test
	public void getAllUser_test(){
		List<Utente> userList = (List<Utente>)FactoryDao.getInstance().getUtenteDao().getByAll(Utente.class);
	}
	
	@Test
	public void updateUser_Test() throws Exception{
		user.setCitta("PROVA");
		user.setIndirizzo("Prova");
		
		FactoryDao.getInstance().getUtenteDao().update(user);
		assertNotNull(user);
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		if((user.getIdUtente()!=0)&&(persona.getIdPersona()!=0)){
			FactoryDao.getInstance().getUtenteDao().delete(user.getIdUtente(), Utente.class);
			FactoryDao.getInstance().getPersonaDao().delete(persona.getIdPersona(), Persona.class);
		}
	}
	
	@After
	public void tearDown() throws Exception {
	}

}
