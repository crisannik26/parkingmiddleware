package it.unisalento.parking.domain.test;

import static org.junit.Assert.*;

import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import it.unisalento.parking.dao.FactoryDao;
import it.unisalento.parking.domain.Pagamento;
import it.unisalento.parking.domain.Parcheggio;
import it.unisalento.parking.domain.Prenota;
import it.unisalento.parking.domain.Utente;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PrenotaDaoTest {
	
	static String targa;
	static int idPark;
	static Prenota prenota;
	static Prenota prenotaOther;
	static Utente user;
	static Parcheggio park;
	static Pagamento pagamento; 
	static Date date;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		targa = "LE527XM";
		idPark = 2;
		prenota = new Prenota();
		user = new Utente();
		park = new Parcheggio();
		pagamento = new Pagamento();
		date = new Date();
		user.setIdUtente(1);
		park.setIdParcheggio(1);
		pagamento.setIdPagamento(1);
		prenotaOther=new Prenota(pagamento, park, user, date, false, targa);
		prenotaOther=new Prenota(pagamento, park, user, date, date, false, targa);
	}
	
	@Before
	public void setUp() throws Exception {
		
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void searchcar_prenota_test() {
		List<Prenota> list = FactoryDao.getInstance().getPrenotaDao().searchcar(targa, idPark);
		for (Iterator iterator = list.iterator(); iterator.hasNext();) {
			Prenota prenota = (Prenota) iterator.next();
			System.out.println(prenota.getParcheggio().getIdParcheggio());
			
		}
		assertTrue(list.size()>0);
		
	}
	
	@Test
	public void createPrenota_Test(){
		prenota.setPagamento(pagamento);
		prenota.setUtente(user);
		prenota.setParcheggio(park);
		
		prenota.setAbbonamento(false);
		prenota.setTarga(targa);
		prenota.setOraInizio(date);
		Calendar orafine = Calendar.getInstance();
		orafine.add(Calendar.HOUR_OF_DAY, 1);
		date=orafine.getTime();
		prenota.setOraFine(date);
		
		FactoryDao.getInstance().getPrenotaDao().save(prenota);
		assertNotNull(prenota);
	}
	
	@Test
	public void getByIdPrenota_test(){
		assertNotNull(FactoryDao.getInstance().getPrenotaDao().getById(prenota.getIdPrenota(), Prenota.class));
	}
	
	@Test
	public void getAllPrenota_Test(){
		List<Prenota> prenotaList =(List<Prenota>)FactoryDao.getInstance().getPrenotaDao().getByAll(Prenota.class);
		assertNotNull(prenotaList.size());
	}
			
//	@Test
//	public void infoPrenotaWithIdPark_Test(){
//		assertNotNull(FactoryDao.getInstance().getPrenotaDao().infoprenotazione(park.getIdParcheggio()));
//	}
	
	@Test
	public void updatePrenota_Test() throws Exception{
		prenota.setAbbonamento(true);
		FactoryDao.getInstance().getPrenotaDao().update(prenota);
		assertNotNull(prenota);
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		if(prenota.getIdPrenota()!=0){
			FactoryDao.getInstance().getPrenotaDao().delete(prenota.getIdPrenota(), Prenota.class);
		}
		prenotaOther=null;
	}


}
