package it.unisalento.parking.domain.test;

import it.unisalento.parking.util.FromStringToDate;
import static org.junit.Assert.*;

import java.util.Date;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class FromStringToDateTest {
	String data_string;
	Date data_date;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		data_string="2017/02/20 22:23:00";
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void convertFromStringToDate_Test() throws Exception {
		data_date = FromStringToDate.convert(data_string);
		assertNotNull(data_date);		
		
	}

}
