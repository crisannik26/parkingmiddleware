package it.unisalento.parking.domain.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import it.unisalento.parking.dao.FactoryDao;
import it.unisalento.parking.domain.Parcheggio;
import it.unisalento.parking.domain.Zona;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ParcheggioDaoTest {
	static Zona zona;
	static Parcheggio park;
	static Parcheggio parkOther;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		zona = new Zona();
		park= new Parcheggio();
		parkOther = new Parcheggio(zona, "camper");
		zona.setIdZona(2);		
	}



	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void createParcheggio_Test(){
		park.setZona(zona);
		park.setTipo("moto");
		FactoryDao.getInstance().getParcheggioDao().save(park);
		assertNotNull(park);
	}
	
	@Test
	public void getByIdParcheggio_Test(){
		assertNotNull(FactoryDao.getInstance().getParcheggioDao().getById(park.getIdParcheggio(), Parcheggio.class));
	}
	
	@Test
	public void getAllParcheggio_Test(){
		List<Parcheggio> parkList =(List<Parcheggio>)FactoryDao.getInstance().getParcheggioDao().getByAll(Parcheggio.class);
		assertNotNull(parkList.size());
	}
	
	@Test
	public void getParcheggioWithIdZone_Test() {
		List<Parcheggio> parkList = FactoryDao.getInstance().getParcheggioDao().infopark(zona.getIdZona());
		assertNotNull(parkList.size());
	}
	
	@Test
	public void updateParcheggio_Test() throws Exception{
		park.setTipo("Nuovo Tipo");
		FactoryDao.getInstance().getParcheggioDao().update(park);
		assertNotNull(park);
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		if(park.getIdParcheggio()!=0){
			FactoryDao.getInstance().getParcheggioDao().delete(park.getIdParcheggio(), Parcheggio.class);
		}
		parkOther=null;
	}

}
