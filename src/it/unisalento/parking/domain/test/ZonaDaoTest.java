package it.unisalento.parking.domain.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import it.unisalento.parking.dao.FactoryDao;
import it.unisalento.parking.domain.Gestore;
import it.unisalento.parking.domain.Zona;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ZonaDaoTest {
	
	static Zona zona;
	static Zona zonaOther;
	static Gestore gestore;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		zona = new Zona();
		zonaOther = new Zona(gestore, "Prova", "Prova", "Prova", 20, 20);
		zonaOther = new Zona(gestore, "info", "info", "info", 60, 60, false, false);
		gestore = new Gestore();
		gestore.setIdGestore(1);
	}

	@Before
	public void setUp() throws Exception {
	}

	

	@Test
	public void idAfterCreateZona_Test() {
		assertNotNull(FactoryDao.getInstance().getZonaDao().idaftercreate());
	}
	
	@Test
	public void extractInfoZonaWithIdGestore_Test(){
		List<Zona> zona =(List<Zona>) FactoryDao.getInstance().getZonaDao().extractinfo(gestore.getIdGestore());
		assertNotNull(zona.size());
	}
	
	@Test
	public void createZona_Test(){
		zona.setGestore(gestore);
		zona.setIndirizzo("Test");
		zona.setFotografia("test");
		zona.setLatitudine(50);
		zona.setLavaggio(false);
		zona.setLongitudine(50);
		zona.setNome("TestName");
		zona.setRistoro(false);
		
		FactoryDao.getInstance().getZonaDao().save(zona);
		assertNotNull(zona);
		
	}
	
	@Test
	public void getByIdZona_test(){
		assertNotNull(FactoryDao.getInstance().getZonaDao().getById(zona.getIdZona(), Zona.class));
	}
	
	@Test
	public void getAllZona_Test(){
		List<Zona> zonaList = (List<Zona>)FactoryDao.getInstance().getZonaDao().getByAll(Zona.class);
		assertNotNull(zonaList.size());
	}
	
	@Test
	public void updateZona_Test() throws Exception{
		zona.setLavaggio(true);
		zona.setRistoro(true);
		
		FactoryDao.getInstance().getZonaDao().update(zona);
		assertNotNull(zona);
	}
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		if(zona.getIdZona()!=0){
			FactoryDao.getInstance().getZonaDao().delete(zona.getIdZona(), Zona.class);
		}
		zonaOther=null;
	}
	
	@After
	public void tearDown() throws Exception {
	}

}
